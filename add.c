#include<stdio.h>
int main()
{
    int i,j,m,n,mat1[10][10],mat2[10][10],sum[10][10];
    printf("Enter the order of the matrices\n");
    scanf("%d%d",&m,&n);
    printf("Enter the elements of the first matrix\n");
    for(i=0;i<m;i++)
    {
        for(j=0;j<n;j++)
        {
            scanf("%d",&mat1[i][j]);
        }
    }
    printf("Enter the elements of the second matrix\n");
    for(i=0;i<m;i++)
    {
        for(j=0;j<n;j++)
        {
            scanf("%d",&mat2[i][j]);
        }
    }
    printf("The sum of the matrices are\n");
    for(i=0;i<m;i++)
    {
        for(j=0;j<n;j++)
        {
            sum[i][j]=mat1[i][j]+mat2[i][j];
            printf("%d\t",sum[i][j]);
        }
        printf("\n");
    }
    return 0;
}