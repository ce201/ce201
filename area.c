#include<stdio.h>
float circle(float r)
{
    float area;
    area=r*r*3.14;
    return area;
}
int main()
{
    float r, area;
    printf("Enter the radius of the circle\n");
    scanf("%f",&r);
    area=circle(r);
    printf("Area of the circle from the given radius %f is %f",r,area);
    return 0;
}