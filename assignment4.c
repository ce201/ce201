#include<stdio.h>
#include<math.h>
int main()
{
	float a,b,c,d,r1,r2;
	printf("Enter the a, b, c values of the quadratic equation");
	scanf("%f%f%f",&a,&b,&c);
	d=(b*b)-(4*a*c);
	switch(d>0)
	{
		case 1:
			r1=(-b+sqrt(d))/(2*a);
			r2=(-b-sqrt(d))/(2*a);
			printf("The roots are real and distinct: %f %f",r1,r2);
			break;
		case 0:
			switch(d<0)
			{
				case 1:
					printf("The roots are imaginary");
					break;
				case 0:
					r1=-b/(2*a);
					printf("The roots are real and equal: %f",r1);
					break;
			}
	}
	return 0;
}