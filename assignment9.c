#include<stdio.h>
int main()
{
	int i,j,row,col,mat[10][10];
	printf("Enter the order of the matrix\n");
	scanf("%d %d",&row,&col);
	printf("Enter the elements of the matrix\n");
	for(i=0;i<row;i++)
	{
		for(j=0;j<col;j++)
		{
			scanf("%d",&mat[i][j]);
		}
	}
	printf("The elements in the matrix are\n");
	for(i=0;i<row;i++)
	{
		for(j=0;j<col;j++)
		{
			printf("%d\t",mat[i][j]);
		}
		printf("\n");
	}
	return 0;
}