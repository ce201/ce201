#include<stdio.h>
#include<math.h>
int main()
{
	float a,b,c,d,r1,r2,e,f;
	printf("Enter the a, b, c values of the quadratic equation");
	scanf("%f%f%f",&a,&b,&c);
	d=(b*b)-(4*a*c);
	if (d>0)
	{
		r1=(-b-sqrt(d))/(2*a);
		r2=(-b+sqrt(d))/(2*a);
		printf("The root are %f and %f",r1,r2);
	}
	else if (d==0)
	{
		r1=(-b)/(2*a);
		printf("The roots are equal and is %f",r1);
	}
	else if (d<0)
	{
		e=-b/(2*a);
		f=sqrt(-d)/(2*a);
		printf("The roots are imaginary and are %f+i%f and %f-i%f",e,f,e,f);
	}
	return 0;
}