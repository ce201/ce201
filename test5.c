#include<stdio.h>
int main()
{
	int marks[5][3],i,j,max;
	for(i=0;i<5;i++)
	{
		printf("Enter the marks obtained by student %d\n",i+1);
		for(j=0;j<3;j++)
		{
			scanf("%d",&marks[i][j]);
		}
	}
	for(j=0;j<3;j++)
	{
		max=marks[0][j];
		for(i=0;i<5;i++)
		{
			if(max<marks[i][j])
				max=marks[i][j];
		}
    printf("The highest marks scored in subject %d is %d\n",j+1,max);
	}
	return 0;
}