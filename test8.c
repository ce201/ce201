#include<stdio.h>
void swap(int *a,int *b)
{
	int p;
	p=*a;
	*a=*b;
	*b=p;
}
int main()
{
	int m,n;
	printf("Enter the two numbers to be swapped\n");
	scanf("%d%d",&m,&n);
	printf("The numbers before swapping:\n a=%d, b=%d\n",m,n);
	swap(&m,&n);
	printf("The numbers after swapping:\n a=%d, b=%d",m,n);
	return 0;
}